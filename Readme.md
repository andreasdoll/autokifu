# autokifu

``autokifu`` parses a game of [go](https://en.wikipedia.org/wiki/Go_(game)) (either live via camera or from a video file) and generates a record (kifu) of this game using the standard [SGF format](http://www.red-bean.com/sgf/).


## Dependencies
* python 2
* opencv (cv2)
* numpy
