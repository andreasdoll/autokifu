#!/usr/bin/python
from __future__ import division, absolute_import
import argparse
import logging
import sys
import os
try:
    import colored_traceback.auto
except ImportError:
    pass

import autokifu
from autokifu import Autokifu

# Argument parser
parser = argparse.ArgumentParser()
parser.add_argument("-v", 
                    action='version', 
                    version='autokifu %s'%autokifu.__version__)
parser.add_argument('file',
                    help='video file to process',
                    nargs='?')       # optional argument
args = vars(parser.parse_args())


# Start application
if args['file'] != None and not os.path.exists(args['file']):
    print "Cannot find file %s"%args['file']
    sys.exit(-1)
else:
    app = Autokifu(args['file'])
    kifu = app.parse()
