from __future__ import division, absolute_import
import logging
import os

logger = logging.getLogger(__name__)

def ask_valid_source():
    stream = _ask_valid_stream_type() 
    if stream == 'camera':
        return 0
    elif stream == 'file':
        return _ask_valid_file_name() 
    else:
        assert False

def _ask_valid_stream_type():
    logger.info('Choose input method:')
    logger.info('[1] from camera')
    logger.info('[2] from file')
    raw_source = raw_input("> ")
    try:
        source = int(raw_source)
    except ValueError:
        logger.error('%s is no valid choice', raw_source)
        return _ask_valid_stream_type()

    if source == 1:
        return 'camera' 
    if source == 2:
        return 'file'
    else:
        logger.error('%s is no valid choice', source)
        return _ask_valid_stream_type()

def _ask_valid_file_name():
    logger.info('Which file to load?')
    fileName = raw_input("> ")
    if os.path.exists(fileName):
        return fileName
    else:
        logger.error('Cannot find %s', fileName)
        return _ask_valid_file_name()


def ask_valid_board_size():
    boardSizes = [19, 13, 9]
    logger.info('Choose board size: %s', boardSizes)
    raw_size = raw_input("> ")
    try:
        size = int(raw_size)
    except ValueError:
        logger.error('No valid choice')
        return ask_valid_board_size()

    if size in boardSizes:
        return size
    else:
        logger.error('No valid choice')
        return ask_valid_board_size()


def ask_game_info(boardSize):
    info = {}
    info['boardSize'] = boardSize 
    return info
