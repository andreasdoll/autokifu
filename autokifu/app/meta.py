from __future__ import division, absolute_import
import logging
from numbers import Integral
import os
import pickle
import cv2

#from autokifu.cv.config import configure_board, configure_colors

logger = logging.getLogger(__name__)


####################################
# Stream configuration
####################################

####################################
# Handle user input
####################################

def parse_user_input(playerB, playerW, boardSize):
    gameInfo = parse_players(playerB, playerW)
    
    if isNumber(boardSize) and int(boardSize) > 0:
        gameInfo['boardSize'] = int(boardSize)
    else:
        logger.error('Invalid boardsize: must be positive integer')
        assert False

    return gameInfo

def parse_players(playerB, playerW):
    def _parse_player(color, string, info):
        if string != None:
            info[color] = {}
            rank = parse_rank(string[-1])
            if rank != None:
                info[color]['rank'] = rank
                if len(string) > 1:
                    info[color]['name'] = ' '.join(string[:-1])
            else:
                info[color]['name'] = ' '.join(string)
        return info

    info = {}
    info = _parse_player('black', playerB, info)
    info = _parse_player('white', playerW, info)
    return info

def parse_rank(rank):
    # kyu
    if rank[-3:] == 'kyu':
        if isNumber(rank[:-3]):
            if int(rank[:-3]) in range(1, 31):
                return str(rank[:-3]+'k')
            else:
                logger.warning('Invalid rank: kyu must be 1-30. Ignoring rank.')
    if rank[-1] == 'k':
        if isNumber(rank[:-1]):
            if int(rank[:-1]) in range(1, 31):
                return str(rank[:-1]+'k')
            else:
                logger.warning('Invalid rank: kyu must be 1-30. Ignoring rank.')

    # dan
    if rank[-3:] == 'dan':
        if isNumber(rank[:-3]):
            if int(rank[:-3]) in range(1, 10):
                return str(rank[:-3]+'d')
            else:
                logger.warning('Invalid rank: dan must be 1-9. Ignoring rank.')
    if rank[-1] == 'd':
        if isNumber(rank[:-1]):
            if int(rank[:-1]) in range(1, 10):
                return str(rank[:-1]+'d')
            else:
                logger.warning('Invalid rank: dan must be 1-9. Ignoring rank.')

    return None

def isNumber(n):
    try:
        n = int(n)
        if isinstance(n, Integral):
            return True 
        else:
            return False
    except ValueError:
        return False 
