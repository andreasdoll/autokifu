from __future__ import division, absolute_import
import cv2
import logging
import time
import os
import pickle

from autokifu.cv.stones import detect_stones
from autokifu.cv.intersections import create_ROI
from autokifu.gui.draw import draw_windows
from autokifu.kifu.Recorder import Recorder
from autokifu.gui.slider import ensure
from autokifu.cv.config import detect_board, calibrate_colors
#import autokifu.gui.window as window

from autokifu.app import io
from autokifu.gui import gui
from autokifu.app.Stream import Stream, FileStream

logger = logging.getLogger(__name__)

class Autokifu(object):
    def __init__(self, fileName=None):
        _setup_logging()
        self._last_detection_time = time.time()

        # setup parsing
        source = _use_or_ask_valid_source(fileName)
        self._stream = Stream(source) if source == 'camera' else FileStream(source)
        self._config = self._configure(source)

        ## setup game
        self._info = io.ask_game_info(self.config['boardSize'])
        self._recorder = Recorder(self.info, self.config['sgfFile'])

    def parse(self):
        logger.info('Start parsing')
        conf = self.config
        stream = self.stream
        recorder = self.recorder

        gui.setup_main(stream, recorder)

        while True:
            redrawSituation = False
            frame, board = stream.grab() 

            k = cv2.waitKey(1)
            if k == 112 or self._detect_this_frame():
                stones = detect_stones(board, 
                                       conf['roi'], 
                                       conf['sb'], 
                                       conf['sw'])
                self.last_detection_time = time.time()

                if k == 112:    # p
                    redrawSituation = recorder.propose_sequence(stones, 
                                                                conf['roi'].keys())
                else:
                    redrawSituation = recorder.handle_changes(stones)

            draw_windows(board, frame, recorder, redrawSituation) 

    def _detect_this_frame(self):
        stream = self.stream
        sec = cv2.getTrackbarPos('Detect each [s]', 'Main')
        sec = ensure(sec, range=[1, 30])

        if stream.type == 'camera':
            now = time.time()
            if now - self.last_detection_time > sec:
                return True
            else:
                return False
        elif stream.type == 'file':
            if stream.frames['current'] % (sec*stream.frames['fps']) == 0:
                return True
            else:
                return False
        else:
            assert False, stream.type

    def _configure(self, source):
        stream = self.stream
        confFile = "%s.conf"%_out_file_name(source) 
        sgfFile = "%s.sgf"%_out_file_name(source) 

        if os.path.exists(confFile):
            logger.info('Using stored configuration')
            conf = _load_config(confFile)
            if not conf['headup']:
                stream.set_warp(conf['warp'])
        else:
            boardSize = 19 #io.ask_valid_board_size()

            logger.info('Configuring stream')
            # board roi
            confBoard = detect_board(stream, boardSize)
            if not confBoard['headup']:
                stream.set_warp(confBoard['warp'])
            # colors
            confColor = calibrate_colors(stream)

            # gather configuration as dict
            conf = {'boardSize': boardSize,
                    'sgfFile': sgfFile}
            for configuration in [confBoard, confColor]:
                for c, v in configuration.iteritems():
                    conf[c] = v

            _store_config(confFile, conf)

        # create intersection rois
        conf['roi'] = create_ROI(conf['intersections'], 
                                 conf['warp']['size'], 
                                 conf['roiRadius'])

        # reset file stream
        if stream.type == 'file':
            stream.set_frameNr(0)
        return conf




    @property
    def info(self):
        return self._info

    @property
    def config(self):
        return self._config

    @property
    def stream(self):
        return self._stream

    @property
    def recorder(self):
        return self._recorder

    @property
    def last_detection_time(self):
        return self._last_detection_time

    @last_detection_time.setter
    def last_detection_time(self, t):
        self._last_detection_time = t

def _load_config(fileName):
    outFile = open(fileName, 'r')
    config = pickle.load(outFile)  
    outFile.close()
    return config 

def _store_config(fileName, conf):
    outFile = open(fileName, 'wb')
    pickle.dump(conf, outFile)
    outFile.close()

def _out_file_name(source):
    if source == 0:
        outName = "webcam"
    else:
        if '/' not in source:
            outName = source
        else:
            outName = source.split('/')[-1]
    return outName

def _use_or_ask_valid_source(fileName):
    if fileName == None:
        return io.ask_valid_source()
    else:
        if os.path.exists(fileName):
            return fileName
        else:
            logger.error('Cannot find file %s', fileName)
            return io.ask_valid_source()

def _setup_logging():
    logging.basicConfig(#filename='game.log',
                        #filemode='w',
                        format='[%(levelname)s] %(message)s',
                        #format='[%(levelname)s] [%(name)s]  %(message)s',
                        #format='[%(levelname)s] [%(funcName)s]  %(message)s',
                        #format='[%(levelname)s] [%(name)s] [%(funcName)s]  %(message)s',
                        #format='[%(asctime)s] [%(levelname)s] [%(name)s] [%(funcName)s]   %(message)s',
                        level=logging.INFO)

