from __future__ import division, absolute_import
import cv2
import logging
import numpy as np

logger = logging.getLogger(__name__)

class Stream(object):
    def __init__(self, source):
        self._type = 'camera' if source == 0 else 'file'
        self._stream = cv2.VideoCapture(source)
        self._warp = None

    @property
    def type(self):
        return self._type

    @property
    def transformed(self):
        return self._warp != None

    def set_warp(self, warp):
        self._warp = warp

    def grab(self):
        _, frame = self._stream.read()

        if not self.transformed:
            return frame, frame
        else:
            warp = self._warp
            boardROI = cv2.warpPerspective(frame, warp['M'], warp['size'])
            return frame, boardROI

class FileStream(Stream):
    @property
    def frames(self):
        return {'fps':     int(self._stream.get(cv2.cv.CV_CAP_PROP_FPS)),
                'current': int(self._stream.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)),
                'total':   int(self._stream.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))}

    def set_frameNr(self, n):
        self._stream.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, int(n))
