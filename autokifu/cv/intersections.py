from __future__ import division, absolute_import
import numpy as np
import cv2
import logging

from autokifu.cv.board import detect_board_contour
from autokifu.util.math import intersect_lines, vector
from autokifu.util.coordinate import np_to_board

logger = logging.getLogger(__name__)

def compute_intersections(frame, warp, blurSize, th1, th2, boardSize):
    warped = cv2.warpPerspective(frame, warp['M'], warp['size'])

    success, _, hull, _ = detect_board_contour(warped,
                                               blurSize,
                                               th1,
                                               th2,
                                               selected=0,
                                               padding=0,
                                               rotation=0)

    if success:
        simpleHull = hull['grid'][:,0,:]

        # gridpoints
        right =  _make_points_on(simpleHull[0], simpleHull[1], boardSize, grid=True)
        bottom = _make_points_on(simpleHull[2], simpleHull[1], boardSize, grid=True)
        left =   _make_points_on(simpleHull[3], simpleHull[2], boardSize, grid=True)
        top =    _make_points_on(simpleHull[3], simpleHull[0], boardSize, grid=True)

        intersections = _intersect_and_label(left, right, top, bottom, boardSize)

        boardPixel = max(vector(simpleHull[0], simpleHull[1]))
        stoneDiameter = boardPixel/boardSize
        roiRadius = int(np.ceil(stoneDiameter/2/2))       # half the radius
        return True, intersections, hull['grid'], roiRadius
    else:
        return False, None, None, None


def _make_points_on(p1, p2, boardSize, grid):
    points = np.zeros((boardSize, 2))
    v = vector(p1, p2)
    if grid:
        for n, s in enumerate(np.linspace(0, 1, boardSize)):
            points[n] = p1 + s*v 
    else:
        pad = 1/(boardSize-1)/2
        for n, s in enumerate(np.linspace(0+pad, 1-pad, boardSize-1)):
            points[n] = p1 + s*v 
    return points

def _intersect_and_label(left, right, top, bottom, boardSize):
    intersections = {}
    for n in range(boardSize):
        pT, pB = top[n], bottom[n]
        for l, (pL, pR) in enumerate(zip(left, right)):
            coord = np_to_board(boardSize-l-1, n)
            intersections[coord] = intersect_lines(pL, pR, pT, pB)
    return intersections

def _separation_mask(warpSize, left, right, top, bottom, nSeparators):
    mask = np.ones(warpSize, dtype=np.uint8) 
    for l, r in zip(left, right):
        l = (0, int(l[1]))
        r = (warpSize[0], int(r[1]))
        cv2.line(mask, l, r, (0,0,0), 1)
    for t, b in zip(top, bottom):
        t = (int(t[0]), 0)
        b = (int(b[0]), warpSize[0])
        cv2.line(mask, t, b, (0,0,0), 1)
    return mask

def create_ROI(intersections, warpSize, radius):
    roi = {}
    for pos, pt in intersections.iteritems():
        mask = np.zeros(warpSize, dtype=np.uint8)
        pt = (int(pt[0]), int(pt[1]))
        cv2.circle(mask, pt, radius, (255,255,255), -1)
        roi[pos] = mask
    return roi
