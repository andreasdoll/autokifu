from __future__ import division, absolute_import
import cv2
import logging

from autokifu.cv.board import detect_board_contour
from autokifu.cv.intersections import compute_intersections
#from autokifu.cv.grab import grab_frame, grab_board
from autokifu.gui import gui
from autokifu.gui.draw import draw_board_roi, draw_intersections
#from autokifu.gui.slider import ensure

logger = logging.getLogger(__name__)

def detect_board(stream, boardSize):
    gui.setup_board_configuration(stream)
    logger.info('Board detection')
    logger.info('Press <Enter> to accept configuration')

    while True:
        frame, _ = stream.grab()
        show = frame.copy()

        # get selection
        # TODO maybe add option to display window?
        blurSize =  3#cv2.getTrackbarPos('blurSize',       'Denoise')
        th1 =       30#cv2.getTrackbarPos('lowerThreshold', 'Denoise')
        th2 =       150#cv2.getTrackbarPos('upperThreshold', 'Denoise')
        selectedC = cv2.getTrackbarPos('contour nr.',    'Contour')
        padding =   cv2.getTrackbarPos('padding',        'Contour')
        rotation =  cv2.getTrackbarPos('rotation',       'Contour')
        # modify selections
        #blurSize = ensure(blurSize, [1,50])


        success, warp, hull, edges = detect_board_contour(frame,
                                                          blurSize,
                                                          th1,
                                                          th2,
                                                          selectedC,
                                                          padding,
                                                          rotation)

        if success:
            contour = draw_board_roi(show, hull)
            warped = cv2.warpPerspective(frame, warp['M'], warp['size'])
            success, intersections, gridHull, roiRadius = compute_intersections(frame, 
                                                                                warp, 
                                                                                blurSize, 
                                                                                th1, 
                                                                                th2, 
                                                                                boardSize)

            if success:
                warped = draw_intersections(warped, intersections, gridHull)
            cv2.imshow('Top', warped)
            #cv2.imshow('Denoise', edges)
            cv2.imshow('Contour', contour)
        else:
            cv2.imshow('Top', frame)
            #cv2.imshow('Denoise', edges)

        # TODO (warp == None) + enter = crash
        k = cv2.waitKey(1)
        if k == 10:     # <Enter>
            cv2.destroyAllWindows()
            return {'warp': warp, 
                    'intersections': intersections, 
                    'roiRadius': roiRadius,
                    'headup': False}

def calibrate_colors(stream):
    gui.setup_color_configuration(stream)
    logger.info('Color calibration')
    logger.info('Press <Enter> to accept configuration')

    while True:
        _, board = stream.grab()
        gray = cv2.cvtColor(board, cv2.COLOR_RGB2GRAY)

        # get selection
        sb = cv2.getTrackbarPos('sensitivity', 'Black')
        sw = cv2.getTrackbarPos('sensitivity', 'White')

        _, black = cv2.threshold(gray, sb, 255, cv2.THRESH_BINARY)
        _, white = cv2.threshold(gray, sw, 255, cv2.THRESH_BINARY)

        cv2.imshow('Black', black)
        cv2.imshow('White', white)

        k = cv2.waitKey(1)
        if k == 10:     # <Enter>
            cv2.destroyAllWindows()
            return {'sb': sb, 'sw': sw}

