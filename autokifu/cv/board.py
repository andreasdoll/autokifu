from __future__ import division, absolute_import
import numpy as np
import cv2
import logging

from autokifu.util.math import intersect_lines, vector

logger = logging.getLogger(__name__)


def detect_board_contour(frame, blurSize, th1, th2, selected, padding, rotation):
    # canny edge
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.Laplacian(gray, cv2.CV_8U)
    gray = cv2.GaussianBlur(gray, (blurSize, blurSize), 0)
    edges = cv2.Canny(gray, th1, th2)
 
    # find contours 
    contours, hierarchy = cv2.findContours(edges, \
                                           cv2.RETR_TREE, \
                                           cv2.CHAIN_APPROX_SIMPLE)
    contours.sort(key=cv2.contourArea, reverse=True)
    
    hull = cv2.convexHull(contours[selected])
    hull = cv2.approxPolyDP(hull, \
                            0.1*cv2.arcLength(hull, True), \
                            True)

    if len(hull) == 4:
        hulls = {}
        warp, gridHull, roiHull = _calculate_transformation(hull, padding, rotation)
        # TODO there seems to be an inconsistency with rotations. Why? Get rid of this workaround.
        if rotation == 1:
            rotation = 3
        elif rotation == 3:
            rotation = 1
        hulls['grid'] = _rotate_rectangular(gridHull, rotation)
        hulls['roi'] = _rotate_rectangular(roiHull, rotation)
        return True, warp, hulls, edges
    else:
        return False, None, None, edges

def _calculate_transformation(hull, padding, rotation):
    sortedHull, warpPts, warpSize = _sort_hull_corners_clockwise(hull, rotation)
    biggerHull = _pad_hull(sortedHull, padding)
    hullPts = np.float32(biggerHull)
    M, _ = cv2.findHomography(hullPts[:,0,:].astype(np.float32), \
                              warpPts.astype(np.float32))
    warp = {'M': M, 'size': warpSize}
    return warp, sortedHull, biggerHull

def _sort_hull_corners_clockwise(hull, rotation):
    [r, l] = np.array_split(hull[hull[:,0, 0].argsort()][::-1], 2)
    r = r[r[:,0,1].argsort()]
    l = l[l[:,0,1].argsort()][::-1]
    sortedHull = np.vstack((r, l))

    s = _find_warped_size(sortedHull)
    warpPts = np.float32([[s, 0],
                          [s, s],
                          [0, s],
                          [0, 0]])
    warpPts = _rotate_rectangular(warpPts, rotation)
    return sortedHull, warpPts, (s,s)

def _rotate_rectangular(warpPts, rotation):
    if rotation == 0:
        return warpPts
    elif rotation == 1:
        return np.vstack([warpPts[3:], warpPts[:3]])
    elif rotation == 2:
        return np.vstack([warpPts[2:], warpPts[:2]])
    elif rotation == 3:
        return np.vstack([warpPts[1:], warpPts[:1]])
    else:
        assert False



def _find_warped_size(hull):
    r = hull[1,0,1] - hull[0,0,1]
    l = hull[2,0,1] - hull[3,0,1]
    t = hull[0,0,0] - hull[3,0,0]
    b = hull[1,0,0] - hull[2,0,0]
    return min(r, l, t, b)

def _pad_hull(hull, pad):
    pad = pad/500
    point = {}
    point[0], point[1], point[2], point[3] = hull[0,0], hull[1,0], hull[2,0], hull[3,0]
    center = intersect_lines(point[0], point[2], point[1], point[3])

    vectors = {}
    minNorm = 1E6
    for nr, pt in point.iteritems():
        v = vector(center, pt)
        vectors[nr] = v
        norm = np.linalg.norm(v)
        if norm < minNorm:
            minNorm = norm

    paddedHull = np.zeros((4,1,2), dtype=hull.dtype)
    for nr, v in vectors.iteritems():
        factor = np.linalg.norm(v)/minNorm
        paddedHull[nr,0] = center + v + v*pad*factor
    return paddedHull
