from __future__ import division, absolute_import
import cv2

def detect_stones(board, rois, sb, sw):
    stones = {'white': [], 'black': []}
    for pos, roi in rois.iteritems():
        hist = cv2.calcHist([board],[1],rois[pos],[256],[0,256])
        color = _determine_color_from_histogram(hist, pos, sb, sw)
        if color != None:
            stones[color].append(pos)
    return stones

# TODO parallelize
def _determine_color_from_histogram(hist, pos, sb, sw):
    maximal = sum(list(hist.flatten())) 

    acc = 0
    for j, h in enumerate(hist):
        acc += h
        if acc > maximal/2: 
            break
    
    if j < sb:
        return 'black'
    elif j > sw:
        return 'white'
    else:
        return None
