from __future__ import division, absolute_import
import numpy as np
import string

def np_to_board(r, c):
    abc = board_letters_uppercase()
    return abc[c]+str(r+1)

def board_to_sgf(coord, boardSize):
    letter = coord[0]
    number = np.abs(int(coord[1:])-boardSize-1)

    fst = ord(letter)-64
    if fst >= 10:       # account for skipping I in board coordinates
        fst = chr(fst+96-1)
    else:
        fst = chr(fst+96)
    snd = chr(number+96)
    return fst+snd

def board_to_np(coord):
    row = int(coord[1:])-1

    letter = coord[0]
    coord = ord(letter) - 64
    if coord >= 10:     # account for skipping I in board coordinates
        coord -= 2
    else:
        coord -= 1
    return row, coord

def board_letters_uppercase():
    abc = list(string.ascii_uppercase)
    del abc[8]
    return ''.join(abc)
