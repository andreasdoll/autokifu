from __future__ import division, absolute_import

colors = ['black', 'white']

def asLetter(c):
    return c[0]

def asWord(c):
    if c == 'b':
        return 'black'
    elif c == 'w':
        return 'white'

def antiColor(c):
    if c == 'black':
        return 'white'
    elif c == 'white':
        return 'black'

def rgb(c):
    if c == 'black':
        return (0, 0, 0)
    elif c == 'white':
        return (255, 255, 255)
