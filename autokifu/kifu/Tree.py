from __future__ import division, absolute_import
from copy import deepcopy
import logging

from autokifu.util.color import antiColor
from autokifu.kifu.sgf import format_game_as_sgf, format_move_as_sgf
from autokifu.kifu.situation import empty_board
from autokifu.kifu.logic import n_new

logger = logging.getLogger(__name__)


class Tree(object):
    def __init__(self, info, sgfFile):
        self.info = info
        self.sgfFile = sgfFile
        self.root = Node('white', 
                         None, 
                         empty_board(info['boardSize']), 
                         {'black': 0, 'white': 0})
        self.activeBranch = []

    @property
    def head(self):
        node = self.root
        for (_, move) in self.activeBranch:
            node = node.get_child(move)
        return node

    @property
    def nMoves(self):
        return len(self.activeBranch)

    def add_move(self, color, coord, situation, prisoners):
        if coord not in self.head.child_coords:
            self.head.add_child(color, coord, situation, prisoners)
        self.activeBranch.append((color, coord))

    def store_to_hd(self):
        sgf_moves = self._moves_as_sgf()
        sgf_game = format_game_as_sgf(self.info, sgf_moves)
        with open(self.sgfFile, "w") as f:
            f.write(sgf_game)

    # TODO store all branches
    def _moves_as_sgf(self):
        node = self.root
        sgf = ""
        for (_, move) in self.activeBranch:
            node = node.get_child(move)
            sgf += format_move_as_sgf(node.color, node.coord, self.info['boardSize'])
        return sgf


    def try_follow_branch_brackward(self, complete, partial, changes):
        stones = self._missing_stones_from_head_downwards(complete, partial)
        if len(stones) > 0:
            self._follow_branch_backward(stones)
            for (color, move) in stones:
                changes[color]['missing'].remove(move)
            return True, changes
        else:
            return False, changes

    def _missing_stones_from_head_downwards(self, complete, partial):
        branch = deepcopy(self.activeBranch)
        missing = []
        while len(branch) > 0:
            (color, head) = branch.pop()
            if head in complete[color] or head in partial[color]:
                missing.append((color, head))
            else:
                break
        return missing

    def _follow_branch_backward(self, rollback):
        N = len(rollback)
        assert set(self.activeBranch[-N:]) == set(rollback)
        self.activeBranch = self.activeBranch[:-N]


    def try_follow_branch_forward(self, changes):
        if n_new(changes) == 0:
            return False, changes

        branches = self._all_forward_branches()
        success, branch = self._longest_forward_branch_to_follow(branches, changes) 
        if success:
            self.activeBranch.extend(branch)
            for (color, move) in branch:
                changes[color]['new'].remove(move)
        return success, changes

    def _all_forward_branches(self):
        originalBranch = deepcopy(self.activeBranch)

        todo = []
        for child in self.head.child_coords:
            activeBranch = deepcopy(self.activeBranch)
            activeBranch.append((self.head.expectedColor, child))
            todo.append(activeBranch)

        branches = self._find_forward_branches(originalBranch, todo, [])

        N = len(originalBranch)
        branches = [b[N:] for b in branches] 
        self.activeBranch = originalBranch
        return branches

    def _longest_forward_branch_to_follow(self, branches, changes):
        measured = [] 
        for branch in branches:
            aborted = False
            for N, (color, move) in enumerate(branch):
                if move not in changes[color]['new']:
                    measured.append(branch[:N])
                    aborted = True
                    break
            if not aborted:
                measured.append(branch)
        
        measured = [b for b in measured if len(b) > 0]
        measured = sorted(measured, key=lambda b: len(b), reverse=True)

        if len(measured) == 0:
            return False, None
        elif len(measured) == 1:
            branch = measured[0]
            return True, branch
        else:
            branch1 = measured[0]
            branch2 = measured[1]
            if len(branch1) > len(branch2):
                return True, branch1
            else:
                logger.warning('It is possible to follow more than one branch. Doing nothing.')
                return False, None

    def _find_forward_branches(self, originalBranch, todo, complete):
        if len(todo) == 0:
            return complete
        else:
            for branch in todo:
                todo.remove(branch)
                self.activeBranch = branch
                if len(self.head.child_coords) > 0:
                    for child in self.head.child_coords:
                        branch = self.activeBranch + [(self.head.expectedColor, child)] 
                        todo.append(branch)
                else:
                    complete.append(branch)
            return self._find_forward_branches(originalBranch, todo, complete)



class Node(object):
    def __init__(self, color, coord, situation, prisoners):
        self.color = color
        self.coord = coord
        self.situation = situation
        self.prisoners = prisoners
        self.children = [] 

    @property
    def expectedColor(self):
        return antiColor(self.color)

    @property
    def child_coords(self):
        return map(lambda c: c.coord, self.children)

    def add_child(self, color, coord, situation, N):
        prisoners = deepcopy(self.prisoners)
        prisoners[color] += N
        newChild = Node(color, coord, situation, prisoners)
        self.children.append(newChild)

    def get_child(self, coord):
        children = [child for child in self.children if child.coord == coord]
        assert len(children) == 1
        return children[0]
