from __future__ import division, absolute_import
import cv2
import logging

from autokifu.kifu.Tree import Tree
from autokifu.kifu.situation import situation_from_detected, diff_situation
from autokifu.kifu.logic import no_changes, illegally_missing, try_figure_out_sequence, test_proposed_sequence
from autokifu.gui.draw import draw_game

logger = logging.getLogger(__name__)

class Recorder(object):
    def __init__(self, gameInfo, sgfFile):
        self.info = gameInfo
        self.tree = Tree(gameInfo, sgfFile)

    def handle_changes(self, stones):
        boardSize = self.info['boardSize']
        tree = self.tree

        detectedSituation = situation_from_detected(stones, boardSize)
        changes = diff_situation(tree.head.situation, detectedSituation)

        if no_changes(changes):
            #logger.info('No changes')
            return False 

        # try handling missing stones
        complete, partial = illegally_missing(changes, tree.head.situation, detectedSituation)
        followedBackward, changes = tree.try_follow_branch_brackward(complete, partial, changes)

        # try handling new stones
        followedForward, changes = tree.try_follow_branch_forward(changes)

        success, moves = try_figure_out_sequence(detectedSituation, changes, tree)
        if success:
            self.commit_sequence(moves)

        if success or followedBackward or followedForward:
            return True
        else:
            return False

    def commit_sequence(self, sequence):
        for move in sequence:
            logger.info('Move #%s: %s at %s', self.tree.nMoves+1, move['color'].title(), move['coord'])
            self.tree.add_move(move['color'], move['coord'], move['situation'], move['prisoners'])
        self.tree.store_to_hd()


    def propose_sequence(self, detectedStones, coordinates):
            situationChanged = False
            logger.info('Input proposed sequence')
            proposed = raw_input("> ").upper()
            if len(proposed) > 0:
                proposed = proposed.split(' ')
                rejected = []
                for move in proposed:
                    if move not in coordinates:
                        rejected.append(move)
                if len(rejected) > 0:
                    logger.info('Sequence rejected: %s is not a valid coordinate', rejected)
                else:
                    situationChanged = self._test_proposed_sequence(proposed, detectedStones)
            else:
                logger.info('No sequence was proposed')
            return situationChanged

    def _test_proposed_sequence(self, proposed, stones):
        boardSize = self.info['boardSize']
        detectedSituation = situation_from_detected(stones, boardSize)

        success, sequence = test_proposed_sequence(proposed, detectedSituation, self.tree)
        if success:
            logger.info('Accepted proposal, continuing')
            self.commit_sequence(sequence)
            return True
        else:
            logger.warning('Proposal was rejected')
            return False
