from __future__ import division, absolute_import
import numpy as np
import logging

from autokifu.util.color import colors, antiColor, asLetter
from autokifu.util.coordinate import board_to_np, np_to_board

logger = logging.getLogger(__name__)

def group_of(r, c, color, situation, libCoord=False):
    assert color == situation[r, c], color
    group, liberties = _traverse_group(color, situation, [(r, c)], [], [])
    if not libCoord:
        return group, len(liberties)
    else:
        return group, liberties

def _traverse_group(color, situation, todo, visited, liberties):
    boardSize = situation.shape[0]
    if len(todo) == 0:
        return visited, liberties
    else:
        (r, c) = todo.pop()
        visited.append((r, c))
        if r != 0:
            if situation[r-1, c] == color:
                if (r-1, c) not in visited and (r-1, c) not in todo:
                    todo.append((r-1, c))
            if situation[r-1, c] == '' and (r-1, c) not in liberties:
                liberties.append((r-1, c))

        if r != boardSize-1:
            if situation[r+1, c] == color:
                if (r+1, c) not in visited and (r+1, c) not in todo:
                    todo.append((r+1, c))
            if situation[r+1, c] == '' and (r+1, c) not in liberties:
                liberties.append((r+1, c))

        if c != 0:
            if situation[r, c-1] == color:
                if (r, c-1) not in visited and (r, c-1) not in todo:
                    todo.append((r, c-1))
            if situation[r, c-1] == '' and (r, c-1) not in liberties:
                liberties.append((r, c-1))

        if c != boardSize-1:
            if situation[r, c+1] == color:
                if (r, c+1) not in visited and (r, c+1) not in todo:
                    todo.append((r, c+1))
            if situation[r, c+1] == '' and (r, c+1) not in liberties:
                liberties.append((r, c+1))

        return _traverse_group(color, situation, todo, visited, liberties)

def adjacent_enemy_groups(r, c, color, situation):
    boardSize = situation.shape[0]
    enemyColor = asLetter(antiColor(color))
    adjacentGroups = []

    if r != 0 and situation[r-1, c] == enemyColor:
        adjacentGroups.append(group_of(r-1, c, enemyColor, situation))
    if r != boardSize-1 and situation[r+1, c] == enemyColor:
        adjacentGroups.append(group_of(r+1, c, enemyColor, situation))
    if c != 0 and situation[r, c-1] == enemyColor:
        adjacentGroups.append(group_of(r, c-1, enemyColor, situation))
    if c != boardSize-1 and situation[r, c+1] == enemyColor:
        adjacentGroups.append(group_of(r, c+1, enemyColor, situation))

    return adjacentGroups

def take_group(group, situation):
    for (r, c) in group:
        situation[r, c] = ''
    return situation, len(group)

def make_move(r, c, color, situation):
    situation[r, c] = color
    return situation

def empty_board(n):
    arr = np.chararray((n, n))
    arr[:,:] = ''
    return arr

def situation_from_detected(stones, boardSize):
    situation = empty_board(boardSize)
    for color, positions in stones.iteritems():
        for pos in positions:
            r, c = board_to_np(pos)
            situation[r, c] = asLetter(color)
    return situation

def diff_situation(situation, detected):
    changes = {}
    for color in colors:
        changes[color] = {'new': [], 'missing': []}

    for (r, c), sit in np.ndenumerate(situation):
        boardCoord = np_to_board(r, c)
        det = detected[r, c]
        if sit != det:
            if sit == '':
                if det == 'b':
                    changes['black']['new'].append(boardCoord)
                elif det == 'w':
                    changes['white']['new'].append(boardCoord)
                else:
                    assert False
            elif sit == 'b':
                if det == '':
                    changes['black']['missing'].append(boardCoord)
                elif det == 'w':
                    changes['black']['missing'].append(boardCoord)
                    changes['white']['new'].append(boardCoord)
                else:
                    assert False
            elif sit == 'w':
                if det == '':
                    changes['white']['missing'].append(boardCoord)
                elif det == 'b':
                    changes['black']['new'].append(boardCoord)
                    changes['white']['missing'].append(boardCoord)
                else:
                    assert False
            else:
                assert False
    return changes
