from __future__ import division, absolute_import
from copy import deepcopy
from itertools import chain
import logging

from autokifu.util.color import antiColor, asLetter, colors
from autokifu.util.coordinate import board_to_np, np_to_board
from autokifu.kifu.situation import make_move, take_group, group_of, adjacent_enemy_groups
from autokifu.kifu.situation import diff_situation

logger = logging.getLogger(__name__)

def n_new(changes, color=None):
    if color != None:
        return len(changes[color]['new'])
    else:
        return len(changes['black']['new']) + len(changes['white']['new'])

def n_mis(changes, color=None):
    if color != None:
        return len(changes[color]['missing'])
    else:
        return len(changes['black']['missing']) + len(changes['white']['missing'])

def reasonable_moves(changes, expectedColor):
    otherColor = antiColor(expectedColor)
    sequence = []

    # one move, correct color
    if n_new(changes, expectedColor) == 1 and \
       n_new(changes, otherColor) == 0:
        sequence.append((expectedColor, changes[expectedColor]['new'][0]))
        return True, sequence

    # two alternating moves
    elif n_new(changes, expectedColor) == 1 and \
         n_new(changes, otherColor) == 1:
        sequence.append((expectedColor, changes[expectedColor]['new'][0]))
        sequence.append((otherColor, changes[otherColor]['new'][0]))
        return True, sequence

    # misdetection of move 
    elif n_new(changes, otherColor) == 1 and \
         n_mis(changes, otherColor) == 1:
        sequence.append((otherColor, changes[otherColor]['new'][0]))
        return True, sequence

    else:
        return False, sequence

def sequence_meets_rules(situation, moveSequence, userInput):
    sequence = []

    for (color, coord) in moveSequence:
        situation = deepcopy(situation)
        r, c = board_to_np(coord)

        if not userInput:
            assert situation[r, c] == ''
        else:
            if situation[r, c] != '':
                return False, sequence

        situation = make_move(r, c, color, situation)
        _, liberties = group_of(r, c, asLetter(color), situation)
        enemyGroups = adjacent_enemy_groups(r, c, color, situation)

        dyingGroups = []
        for (enemyGroup, enemyLiberties) in enemyGroups:
            if enemyLiberties == 0:
                dyingGroups.append(enemyGroup)

        if liberties > 0 and len(dyingGroups) == 0:
            sequence.append({'color': color, 'coord': coord, 'situation': situation, 'prisoners': 0})
        elif liberties == 0 and len(dyingGroups) == 0:
            return False, sequence
        elif len(dyingGroups) > 0:
            prisoners = 0
            for dyingGroup in dyingGroups:
                situation, taken = take_group(dyingGroup, situation)
                prisoners += taken
            assert prisoners > 0
            sequence.append({'color': color, 'coord': coord, 'situation': situation, 'prisoners': prisoners})
        else:
            assert False
    return True, sequence

def no_changes(changes):
    for color in colors:
        if n_new(changes, color) > 0 or n_mis(changes, color) > 0:
            return False
    return True

def try_figure_out_sequence(detectedSituation, changes, tree):
    situation = deepcopy(tree.head.situation)
    expectedColor = deepcopy(tree.head.expectedColor)

    if no_changes(changes):
        return False, None

    if n_new(changes) > 2:
        logger.debug('Dubious changes, cannot handle more than 2 new stones')
        return False, None

    success, moves = reasonable_moves(changes, expectedColor)
    if not success:
        logger.debug('Cannot determine reasonable sequence')
        return False, None

    legal, sequence = sequence_meets_rules(situation, moves, userInput=False)
    if not legal:
        logger.debug('Determined sequence is illegal')
        return False, None

    remnant = diff_situation(sequence[-1]['situation'], detectedSituation)
    if not n_new(remnant) + n_mis(remnant) == 0:
        logger.debug('Determined sequence doesn\'t match detection: %s', remnant)
        return False, None
    else:
        return True, sequence

def test_proposed_sequence(proposed, detectedSituation, tree):
    situation = deepcopy(tree.head.situation)
    color = deepcopy(tree.head.expectedColor)

    moves = [] 
    for move in proposed:
        moves.append((color, move))
        color = antiColor(color)
    
    legal, sequence = sequence_meets_rules(situation, moves, userInput=True)
    if not legal:
        logger.warning('Proposed sequence is illegal')
        return False, None

    remnant = diff_situation(sequence[-1]['situation'], detectedSituation)
    if not n_new(remnant) + n_mis(remnant) == 0:
        logger.warning('Proposed sequence doesn\'t match detection: %s', remnant)
        return False, None
    else:
        return True, sequence

def _groups_which_miss_stones(changes, situation):
    # find partially or completely missing groups
    partially = {'black': [], 'white': []}
    complete  = {'black': [], 'white': []}

    for color in colors:
        for coord in changes[color]['missing']:
            # find group which contained the missing stone
            r, c = board_to_np(coord)
            group, _ = group_of(r, c, asLetter(color), situation)
            group = map(lambda c: np_to_board(*c), group)
            # if the group wasn't recognized yet
            if group not in complete[color] and group not in partially[color]:
                # check if group is partially or completely missing
                partiallyMissing = False
                missingStones = []
                for stone in group:
                    if stone in changes[color]['missing']:
                        missingStones.append(stone)
                    else:
                        partiallyMissing = True
                if partiallyMissing:
                    partially[color].append((group, missingStones))
                else:
                    complete[color].append(group)
    return partially, complete

def _missing_complete_groups(groups, situation, detected):
    legal   = {'black': [], 'white': []}
    illegal = {'black': [], 'white': []}

    for color in colors:
        for group in groups[color]:
            r, c = board_to_np(group[0])
            _, liberties = group_of(r, c, asLetter(color), situation, libCoord=True)
            stillHasLiberties = False
            for (r, c) in liberties:
                if detected[r, c] == '':
                    stillHasLiberties = True
                    break
            if stillHasLiberties:
                illegal[color].append(group)
            else:
                legal[color].append(group)
    return legal, illegal

def illegally_missing(changes, situation, detected):
    partial, complete = _groups_which_miss_stones(changes, situation)
    legal, illegal = _missing_complete_groups(complete, situation, detected)

    illegal = {color: flatten(groups) for color, groups in illegal.iteritems()}
    partial = {color: flatten(groups) for color, groups in partial.iteritems()}
    return illegal, partial

def flatten(l):
    return list(chain.from_iterable(l))
