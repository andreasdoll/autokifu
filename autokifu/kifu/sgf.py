from __future__ import division, absolute_import

import autokifu
from autokifu.util.coordinate import board_to_sgf

def format_game_as_sgf(gameInfo, moves):
    begin = "(; \n"
    setup = _game_info(gameInfo)
    end = ")"
    return begin + setup + moves + end

def format_move_as_sgf(color, coord, boardSize):
    coord = board_to_sgf(coord, boardSize)
    if color == 'black':
        return ";B[%s] \n"%coord
    elif color == 'white':
        return ";W[%s] \n"%coord
    else:
        assert False

def _game_info(info):
    # properties properties
    properties =  "FF[4] \n"                                  # sgf version
    properties += "GM[1] \n"                                  # game of go
    properties += "SZ[%s]\n"%info['boardSize']                # boardSize
    properties += "AP[autokifu:%s]\n"%autokifu.__version__    # creating app

    # game info properties
    # TODO GN, date, komi, result
    # see http://www.red-bean.com/sgf/user_guide/index.html#why_gameinfo
    if 'black' in info.keys():
        if 'name' in info['black'].keys():
            properties += "PB[%s] \n"%info['black']['name']        # player black
        if 'rank' in info['black'].keys():
            properties += "BR[%s] \n"%info['black']['rank']        # black rank

    if 'white' in info.keys():
        if 'name' in info['white'].keys():
            properties += "PW[%s] \n"%info['white']['name']        # player white
        if 'rank' in info['white'].keys():
            properties += "WR[%s] \n"%info['white']['rank']        # white rank

    return properties
