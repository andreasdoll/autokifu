from __future__ import division, absolute_import
import cv2
import numpy as np
import logging
import itertools

from autokifu.util.coordinate import board_letters_uppercase, np_to_board
from autokifu.util.color import antiColor, rgb, colors, asWord
from autokifu.gui.slider import boolean

logger = logging.getLogger(__name__)

def draw_game(recorder):
    board, intersections, stoneSize = _draw_empty_board(recorder.info['boardSize'])
    board = _draw_situation(board, intersections, recorder, stoneSize)
    board = draw_coordinates(board, intersections, recorder.info['boardSize'])
    board = _draw_info(board, recorder)
    return board

def draw_coordinates(frame, intersections, boardSize):
    lastLetter = board_letters_uppercase()[boardSize-1]
    font = cv2.FONT_HERSHEY_SIMPLEX
    thickness = 2
    fontColor = rgb('black') 
    coordPadding = {9: 100,
                    13: 75,
                    19: 50}
    fontSize = {9: 1,
                13: 0.9,
                19: 0.8}

    for coord, pos in intersections.iteritems():
        letter = coord[0]
        number = coord[1:]
        pos = tuple(map(int, pos))
        pos = _center_at(pos, number, font, fontSize[boardSize], thickness)
        if letter == 'A':                   # left
            cv2.putText(frame,
                        number,
                        (pos[0]-coordPadding[boardSize], pos[1]),
                        font, fontSize[boardSize], fontColor, thickness=thickness)
        elif letter == lastLetter:          # right
            cv2.putText(frame,
                        number,
                        (pos[0]+coordPadding[boardSize], pos[1]),
                        font, fontSize[boardSize], fontColor, thickness=thickness)
    for coord, pos in intersections.iteritems():
        letter = coord[0]
        number = coord[1:]
        pos = tuple(map(int, pos))
        pos = _center_at(pos, letter, font, fontSize[boardSize], thickness)
        if int(number) == 1:                # bottom
            cv2.putText(frame,
                        letter,
                        (pos[0], pos[1]+coordPadding[boardSize]),
                        font, fontSize[boardSize], fontColor, thickness=thickness)
        elif int(number) == boardSize:      # top
            cv2.putText(frame,
                        letter,
                        (pos[0], pos[1]-coordPadding[boardSize]),
                        font, fontSize[boardSize], fontColor, thickness=thickness)
    return frame

def draw_board_roi(show, hull):
    # grid roi 
    cv2.drawContours(show,
                    [hull['grid'][1:3]],
                     0,
                     (0,255,255),
                     20)

    cv2.drawContours(show,
                     [hull['grid']],
                     0,
                     (0,0,255),
                     3)


    # padded roi 
    cv2.drawContours(show, \
                     [hull['roi']], \
                     0, \
                     (255,255,0), \
                     3)

    #for nr, pt in enumerate(hull['roi']):
    #    pt = tuple(map(int, pt[0]))
    #    cv2.circle(show, pt, 5, (255,255,0), -1)
    #    cv2.putText(show, \
    #                str(nr), \
    #                pt, \
    #                cv2.FONT_HERSHEY_DUPLEX, \
    #                0.8, \
    #                (255,255,0))
    return show

def draw_intersections(show, intersections, gridContour=None):
    # intersections
    for pos, pt in intersections.iteritems():
        pt = tuple(map(int, pt))
        cv2.circle(show, pt, 3, (0,0,255), -1)
        #cv2.putText(show, \
        #            pos, \
        #            pt, \
        #            cv2.FONT_HERSHEY_SIMPLEX, \
        #            1, \
        #            (255,0,0))

    # contour of grid
    if type(gridContour) != None:
        cv2.drawContours(show, \
                         [gridContour], \
                         0, \
                         (0,0,255), \
                         2)
    return show

def draw_intersection_rois(frame, lines, intersections):
    for rho, theta in lines:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
    
        cv2.line(frame, \
                 (x1,y1), \
                 (x2,y2), \
                 (0,255,255),\
                 1) 

    for pos, pt in intersections.iteritems():
        pt = tuple(map(int, pt))
        cv2.circle(frame, pt, 2, (255,255,0), -1)
    return frame

def _draw_empty_board(boardSize):
    h, w = 1500, 1500

    stoneSize = int(h/(boardSize+2)/2)
    hoshiSize = int(stoneSize/5)

    pos_h = np.linspace(0, h, boardSize+2)
    pos_w = np.linspace(0, w, boardSize+2)

    board = np.zeros((h, w, 3), np.uint8)
    board[:,:,:] = (0,100,200)

    black = rgb('black')

    # draw grid
    for n, x in enumerate(pos_w[1:-1]):
        pt1 = (int(pos_h[1]), int(x))
        pt2 = (int(pos_h[boardSize]), int(x))
        cv2.line(board, pt1, pt2, black, 2)
    for n, y in enumerate(pos_h[1:-1]):
        pt1 = (int(y), int(pos_w[1]))
        pt2 = (int(y), int(pos_w[boardSize]))
        cv2.line(board, pt1, pt2, black, 2)

    # draw hoshi
    hoshiPositions = {9:  list(itertools.product([2, 6], repeat=2)) + [(4, 4)],
                      13: list(itertools.product([3, 9], repeat=2)) + [(6, 6)],
                      19: list(itertools.product([3, 9, 15], repeat=2))}

    for r, y in enumerate(pos_h[1:-1]):
        for c, x in enumerate(pos_w[1:-1]):
            if (r, c) in hoshiPositions[boardSize]:
                pt = (int(y), int(x))
                cv2.circle(board, pt, hoshiSize, (0,0,0), -1)

    intersections = {}
    for c, x in enumerate(pos_w[1:-1]):
        for r, y in enumerate(pos_h[1:-1]):
            intersections[np_to_board(boardSize-r-1, c)] = (x, y)

    return board, intersections, stoneSize

def _draw_situation(frame, intersections, recorder, stoneSize):
    head = recorder.tree.head

    # moves
    for (r, c), color in np.ndenumerate(head.situation):
        coord = np_to_board(r, c)
        pt = tuple(map(int, intersections[coord]))
        if color != '':
	    cv2.circle(frame, pt, stoneSize, rgb(asWord(color)), -1)

    # mark last move
    if recorder.tree.nMoves > 0:
        font = cv2.FONT_HERSHEY_SIMPLEX
        fontSize = 1
        thickness = 2

        nMoves = str(recorder.tree.nMoves)
        pt = _center_at(intersections[head.coord], nMoves, font, fontSize, thickness)
        cv2.putText(frame,
                    nMoves,
                    pt,
                    font,
                    fontSize,
                    rgb(antiColor(head.color)),
                    thickness=thickness)
        #cv2.circle(frame, pt, int(stoneSize/2), rgb(antiColor(color)), 3)
    return frame

def _draw_info(frame, recorder):
    frame, height = _draw_info_banners(frame)
    frame = _write_info_on_banner(frame, height, recorder)
    return frame

def _draw_info_banners(frame):
    height = int(np.round(frame.shape[0]/20))
    width = frame.shape[1]
    bar = np.ones((height, width, 3), dtype=frame.dtype)
    padded = np.concatenate([255*bar, frame, 0*bar], axis=0)
    return padded, height

def _write_info_on_banner(frame, barHeight, recorder):
    game = recorder.info

    padding = int(np.round(barHeight/2))
    width = frame.shape[1]
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontSize = 1
    thickness = 2

    for color in colors:
        fontColor = rgb(antiColor(color))
        boundaryColor = rgb(color)
        if color == 'black':
            y = frame.shape[0] - padding
            y_text = y + 10
        elif color == 'white':
            y = padding
            y_text = y + 10

        # player info
        if color in game.keys():
            if 'name' in game[color].keys() and 'rank' in game[color].keys():
                player = "%s (%s)"%(game[color]['name'], game[color]['rank'])
            elif 'name' in game[color].keys():
                player = game[color]['name']
            elif 'rank' in game[color].keys():
                player = game[color]['rank']
            cv2.putText(frame,
                        player,
                        (40, y_text),
                        font,
                        fontSize,
                        fontColor,
                        thickness=thickness)

        # prisoners
        cv2.circle(frame, (width-163, y), 17, fontColor, -1)
        cv2.circle(frame, (width-163, y), 17, boundaryColor, 2)
        cv2.circle(frame, (width-145, y), 17, fontColor, -1)
        cv2.circle(frame, (width-145, y), 17, boundaryColor, 2)
        cv2.circle(frame, (width-127, y), 17, fontColor, -1)
        cv2.circle(frame, (width-127, y), 17, boundaryColor, 2)

        cv2.putText(frame,
                    ": %s"%int(recorder.tree.head.prisoners[color]),
                    (width-105, y_text),
                    font,
                    fontSize,
                    fontColor,
                    thickness=thickness)

    return frame

def _center_at(pt, text, font, fontSize, thickness):
    size, _ = cv2.getTextSize(text, font, fontSize, thickness)
    pt = (int(pt[0]-size[0]/2), int(pt[1]+size[1]/2))
    return pt

def draw_windows(board, frame, recorder, situationChanged):
    if boolean(cv2.getTrackbarPos('Show detection', 'Main')) and situationChanged:
        cv2.imshow('Detection', draw_game(recorder))
    if boolean(cv2.getTrackbarPos('Show headup', 'Main')):
        cv2.imshow('Headup', board)
    if boolean(cv2.getTrackbarPos('Show stream', 'Main')):
        cv2.imshow('Stream', frame)
