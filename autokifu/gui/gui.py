from __future__ import division, absolute_import
import cv2

from autokifu.gui.slider import nothing
from autokifu.gui.draw import draw_game

def setup_main(stream, recorder):
    cv2.namedWindow('Main', cv2.WINDOW_NORMAL)
    cv2.createTrackbar('Show detection', 'Main', 1, 1, lambda n: _raise_or_destroy('Detection', n))
    cv2.createTrackbar('Show headup', 'Main', 1, 1, lambda n: _raise_or_destroy('Headup', n))
    cv2.createTrackbar('Show stream', 'Main', 0, 1, lambda n: _raise_or_destroy('Stream', n))
    cv2.createTrackbar('Detect each [s]', 'Main', 1, 30, nothing)

    cv2.namedWindow('Detection', cv2.WINDOW_NORMAL)
    cv2.namedWindow('Headup', cv2.WINDOW_NORMAL)
    cv2.imshow('Detection', draw_game(recorder))

    if stream.type == 'file':
        cv2.createTrackbar('time', 
                           'Main', 
                           0, 
                           stream.frames['total'], 
                           lambda n: stream.set_frameNr(n))

def setup_board_configuration(stream):
    #cv2.namedWindow('Denoise', cv2.WINDOW_NORMAL)
    #cv2.createTrackbar('blurSize',       'Denoise', 3, 50,  nothing)
    #cv2.createTrackbar('lowerThreshold', 'Denoise', 30, 400, nothing)
    #cv2.createTrackbar('upperThreshold', 'Denoise', 150, 400, nothing)

    cv2.namedWindow('Contour', cv2.WINDOW_NORMAL)
    cv2.createTrackbar('contour nr.', 'Contour', 0, 5, nothing)
    cv2.createTrackbar('padding',     'Contour', 10, 100, nothing)
    cv2.createTrackbar('rotation',    'Contour', 0, 3, nothing)

    cv2.namedWindow('Top', cv2.WINDOW_NORMAL)

    if stream.type == 'file':
        _setup_time_scrollbar(stream)


def setup_color_configuration(stream):
    cv2.namedWindow('Black', cv2.WINDOW_NORMAL)
    cv2.createTrackbar('sensitivity', 'Black', 110, 255, nothing)

    cv2.namedWindow('White', cv2.WINDOW_NORMAL)
    cv2.createTrackbar('sensitivity', 'White', 225, 255, nothing)

    if stream.type == 'file':
        _setup_time_scrollbar(stream)

def _setup_time_scrollbar(stream):
    cv2.namedWindow('Video position', cv2.WINDOW_NORMAL)
    cv2.createTrackbar('time', 
                       'Video position', 
                       0, 
                       stream.frames['total'], 
                       lambda n: stream.set_frameNr(n))

def _raise_or_destroy(name, val):
    assert val in [0, 1], val
    if val == 0:
        cv2.destroyWindow(name)
    else:
        cv2.namedWindow(name, cv2.WINDOW_NORMAL)
