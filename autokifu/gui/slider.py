from __future__ import division, absolute_import

def ensure(val, range=None, oddness=False):
    if not oddness and range == None:
        return val
    elif not oddness and range != None:
        return _ensure_range(val, range)
    elif oddness and range == None:
        return _ensure_odd(val)
    elif oddness and range != None:
        return _ensure_odd(val, range)

def is_odd(n):
    if n % 2 == 0:
        return False 
    else:
        return True

def nothing(x):
    pass

def _ensure_range(val, range):
    if val < range[0]:
        return range[0]
    elif val > range[1]:
        return range[1]
    else:
        return val

def _ensure_odd(val, range=None):
    if range == None:
        if is_odd(val):
            return val
        else:
            return val + 1
    else:
        inRange = _ensure_range(val, range)
        if is_odd(inRange):
            return inRange
        else:
            if inRange == range[1]:
                return inRange - 1
            else:
                return inRange + 1

def boolean(val):
    assert val in [0, 1], val
    return True if val == 1 else False
