from setuptools import setup, find_packages

setup(
    name='autokifu',
    description='Automatic record generation for go games',
    long_description='autokifu parses a game of go (either live via camera or from a video file) and generates a record (kifu) of this game using the standard SGF format',
    version='0.1',
    author='Andreas Doll',
    author_email='fuseki@posteo.de',
    url='http://bitbucket.org/andreasdoll/autokifu',
    #license='',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: End Users/Desktop',
        # "License :: OSI Approved :: MIT License",
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Games/Entertainment :: Board Games',
	'Topic :: Multimedia :: Video :: Capture'
	'Topic :: Scientific/Engineering :: Image Recognition',
        ],
    keywords='go, baduk, weiqi, game, sgf, kifu, record, capture, video, webcam',
    install_requires=[
        'setuptools',
	'cv2',
	'numpy'
        ],
    packages=find_packages(),
    )
